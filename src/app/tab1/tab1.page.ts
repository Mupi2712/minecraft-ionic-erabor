import {Component} from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';


@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    port = 10460
    ip = '5.83.172.105'
    motd = "";
    icon = "";
    serveronline = true;
    playeronline = 0;
    playermax = 0;
    serverver = "n.a";
    skins ="";
    playersArray: Array<Object>;

    constructor(private http: HttpClient) {
        let url = `https://mcapi.us/server/status?ip=${this.ip}&port=${this.port}`;

        this.http.get(url).subscribe((response) => {
            console.log(response);
            let responseJson = JSON.parse(JSON.stringify(response));
            this.motd = responseJson.motd;
            this.playermax = responseJson.players.max;
            this.playeronline = responseJson.players.now;
            this.serverver = responseJson.server.name;
            this.serveronline = responseJson.online;
            this.icon = responseJson.favicon;
            if (responseJson.players.sample.length > 0){
                this.playersArray = responseJson.players.sample;
                console.log(this.playersArray)
                for (let _i = 0; _i < responseJson.players.sample.length; _i++){
                    // @ts-ignore
                    this.skins = this.skins + "<img src=\"https://minotar.net/armor/body/"+this.playersArray[_i].id+"\">"
                }
                console.log(this.skins)
            }
        });

    }
}
